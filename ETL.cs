
using System;
using System.Collections.Generic;



namespace ETL
{


	public class ETL
	{	

		public static Dictionary<string, int> Transform( Dictionary<int, IList<string>> dictInput )
		{					
			Dictionary<string, int> dictionary = new Dictionary<string, int>();
	
			foreach (KeyValuePair<int, IList<string>> pair in dictInput) 
			{	
				foreach(var value in pair.Value)
				{	
					dictionary.Add(value.ToLower(), pair.Key);	
				}			
			}	
			return dictionary;
		}
	}
}


